package com.hendisantika.moviesneo4j;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MoviesNeo4jApplication {

    public static void main(String[] args) {
        SpringApplication.run(MoviesNeo4jApplication.class, args);
    }
}
