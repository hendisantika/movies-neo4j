package com.hendisantika.moviesneo4j.repository;

import com.hendisantika.moviesneo4j.entity.Person;
import org.springframework.data.neo4j.repository.Neo4jRepository;

/**
 * Created by IntelliJ IDEA.
 * Project : movies-neo4j
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 29/08/18
 * Time: 21.47
 * To change this template use File | Settings | File Templates.
 */
public interface PersonRepository extends Neo4jRepository<Person, Long> {

    Person findByName(String name);

}